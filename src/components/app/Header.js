import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, Link } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  title: {
    flexGrow: 1
  },
  links: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2)
  }
}));

export default function Header() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <Link component={RouterLink} to="/" color="inherit">
              Marvel
            </Link>
          </Typography>
          <Link className={classes.links} component={RouterLink} to="/comics" color="inherit">
            Comics
          </Link>
          <Link className={classes.links} component={RouterLink} to="/creators" color="inherit">
            Creators
          </Link>
        </Toolbar>
      </AppBar>
      <Toolbar />
    </div>
  );
}
