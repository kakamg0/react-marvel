import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Pagination as MaterialPagination } from '@material-ui/lab';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    margin: theme.spacing(2)
  }
}));

export default function Pagination({ list, onChange }) {
  const classes = useStyles();
  return (
    <MaterialPagination
      className={classes.root}
      page={list.offset / list.limit + 1}
      count={Math.floor(list.total / list.limit)}
      shape="rounded"
      onChange={onChange}
    />
  );
}
