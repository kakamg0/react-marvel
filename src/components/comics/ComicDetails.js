import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { format } from 'date-fns';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, CardContent, Typography, ListItem, ListItemAvatar, ListItemText, Avatar, Divider, Link } from '@material-ui/core';
import {
  EventAvailable as EventAvailableIcon,
  Create as CreateIcon,
  Image as ImageIcon,
  BorderColor as BorderColorIcon
} from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.background.paper
  },
  details: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    flex: '1 0 auto'
  },
  media: {
    width: 350,
    display: 'block',
    margin: 'auto',
    marginBottom: theme.spacing(2)
  }
}));

export default function ComicDetail({ object }) {
  const classes = useStyles();
  const publishedDate = object?.dates?.find(d => d.type === 'onsaleDate');
  const writers = object?.creators?.items?.filter(el => el.role === 'writer');
  const artists = object?.creators?.items?.filter(el => el.role === 'penciller (cover)');
  const pencilers = object?.creators?.items?.filter(el => el.role === 'penciller');

  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Grid item xs={12} sm={4}>
        <img
          className={classes.media}
          src={`${object?.thumbnail?.path}.${object?.thumbnail?.extension}`}
          alt={object.title}
          title={object.title}
        />
      </Grid>
      <Grid item xs={12} sm={8}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography variant="subtitle1" color="textSecondary">
              {object.description}
            </Typography>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
              className={classes.root}
            >
              <PublishedDate date={publishedDate} />
              <Creator list={writers} label="Writer" avatar={<CreateIcon />} />
              <Creator list={artists} label="Cover Artist" avatar={<ImageIcon />} />
              <Creator list={pencilers} label="Penciler"  avatar={<BorderColorIcon />} />
            </Grid>
          </CardContent>
        </div>
      </Grid>
    </Grid>
  );
}

function PublishedDate({ date }) {
  if (!date) {
    return null;
  }
  const formatted = format(new Date(date?.date), 'MMMM dd, yyyy');
  return (
    <Grid item xs={12} sm={6}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar>
            <EventAvailableIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Published" secondary={formatted} />
      </ListItem>
      <Divider variant="inset" />
    </Grid>
  );
}

function Creator({ list, label, avatar }) {
  if (!list || list.length === 0) {
    return null;
  }
  const creators = list.map(el => {
    const id = el.resourceURI.split('/').pop();
    return (
      <Link key={id} component={RouterLink} to={`/creators/${id}`} color="inherit">
        {el.name}
      </Link>
    );
  }).reduce((el, link) => el === null ? link : <>{el}, {link}</>, null);
  return (
    <Grid item xs={12} sm={6}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar>
            {avatar}
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary={label} secondary={creators} />
      </ListItem>
      <Divider variant="inset" />
    </Grid>
  );
}
