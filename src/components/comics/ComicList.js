import React from 'react';
import { Grid } from '@material-ui/core';
import ComicItem from './ComicItem';

export default function ComicList({ list }) {
  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="flex-start"
    >
      {list.map(comic => <ComicItem key={comic.id} object={comic} />)}
    </Grid>
  );
}
