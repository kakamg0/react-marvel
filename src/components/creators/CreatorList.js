import React from 'react';
import { Grid } from '@material-ui/core';
import CreatorItem from './CreatorItem';

export default function CreatorList({ list }) {
  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="flex-start"
    >
      {list.map(comic => <CreatorItem key={comic.id} object={comic} />)}
    </Grid>
  );
}
