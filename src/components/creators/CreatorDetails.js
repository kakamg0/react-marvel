import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import ComicList from '../comics/ComicList';

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.background.paper
  },
  media: {
    width: 350,
    display: 'block',
    margin: 'auto',
    marginBottom: theme.spacing(2)
  }
}));

export default function CreatorDetail({ object, comics }) {
  const classes = useStyles();

  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Grid item xs={12} sm={4}>
        <img
          className={classes.media}
          src={`${object?.thumbnail?.path}.${object?.thumbnail?.extension}`}
          alt={object.fullName}
          title={object.fullName}
        />
      </Grid>
      <Grid item xs={12} sm={8}>
        <ComicList list={comics} />
      </Grid>
    </Grid>
  );
}
