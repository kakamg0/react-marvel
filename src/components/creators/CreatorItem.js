import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Grid, Card, CardActionArea, CardMedia, CardContent, Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  card: {
    margin: theme.spacing(2),
    maxWidth: 345
  },
  media: {
    height: 140
  }
}));


export default function CreatorItem({ object }) {
  const classes = useStyles();
  return (
    <Grid item xs={6} sm={3}>
      <Link component={RouterLink} to={`/creators/${object.id}`} color="inherit">
        <Card className={classes.card}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={`${object.thumbnail.path}.${object.thumbnail.extension}`}
              title={object.fullName} />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h3">
                {object.firstName}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
}
