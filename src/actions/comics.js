import HttpClient from '../helpers/http-client';

export const OBJECT_INVALID = 'OBJECT_INVALID_COMICS';

export const OBJECT_FETCH = 'OBJECT_FETCH_COMICS';
export const OBJECT_FETCH_PENDING = 'OBJECT_FETCH_COMICS_PENDING';
export const OBJECT_FETCH_REJECTED = 'OBJECT_FETCH_COMICS_REJECTED';
export const OBJECT_FETCH_FULFILLED = 'OBJECT_FETCH_COMICS_FULFILLED';

export const OBJECT_LIST = 'OBJECT_LIST_COMICS';
export const OBJECT_LIST_PENDING = 'OBJECT_LIST_COMICS_PENDING';
export const OBJECT_LIST_REJECTED = 'OBJECT_LIST_COMICS_REJECTED';
export const OBJECT_LIST_FULFILLED = 'OBJECT_LIST_COMICS_FULFILLED';

const path = 'comics';

export function fetch (id) {
  return {
    type: OBJECT_FETCH,
    payload: HttpClient.get(`${path}/${id}`)
  };
}

export function list (page = 1, limit = 20) {
  const offset = (page - 1) * limit;
  const params = {
    params: {
      limit,
      offset
    }
  };

  return {
    type: OBJECT_LIST,
    payload: HttpClient.get(`${path}`, params)
  };
}

export function listByCreator (id, page = 1, limit = 20) {
  const offset = (page - 1) * limit;
  const params = {
    params: {
      limit,
      offset
    }
  };

  return {
    type: OBJECT_LIST,
    payload: HttpClient.get(`/creators/${id}/comics`, params)
  };
}

export default {
  fetch,
  list,
  listByCreator
};
