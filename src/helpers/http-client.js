import axios from 'axios';

const HttpClient = axios.create({
  baseURL: process.env.REACT_APP_MARVEL_API_HOST,
  params: {
    apikey: process.env.REACT_APP_MARVEL_API_KEY
  },
  responseType: 'json'
});

export default HttpClient;
