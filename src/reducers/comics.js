import {
  OBJECT_INVALID,
  OBJECT_FETCH_PENDING,
  OBJECT_FETCH_REJECTED,
  OBJECT_FETCH_FULFILLED,
  OBJECT_LIST_PENDING,
  OBJECT_LIST_REJECTED,
  OBJECT_LIST_FULFILLED,
} from '../actions/comics'

const initialState = {
  readyState: OBJECT_INVALID,
  list: {
    offset: 0,
    limit: 20,
    total: 0,
    count: 0,
    results: []
  },
  object: {},
  error: false,
  status: null,
  errorObject: null
};

const CRUD_ACTION_HANDLERS = {
  [OBJECT_LIST_PENDING]: (state, action) => {
    return { ...state, readyState: OBJECT_LIST_PENDING, error: false };
  },
  [OBJECT_LIST_REJECTED] : (state, action) => {
    return { ...state, readyState: OBJECT_LIST_REJECTED, error: action.error, status: action.payload.response.status, errorObject: action.payload.response.data };
  },
  [OBJECT_LIST_FULFILLED] : (state, action) => {
    return { ...state, readyState: OBJECT_LIST_FULFILLED, error: false, list: action.payload.data.data, status: null, errorObject: null };
  },

  [OBJECT_FETCH_PENDING]: (state, action) => {
    return { ...state, readyState: OBJECT_FETCH_PENDING, error: false };
  },
  [OBJECT_FETCH_REJECTED]: (state, action) => {
    return { ...state, readyState: OBJECT_FETCH_REJECTED, error: action.error, status: action.payload.response.status, errorObject: action.payload.response.data };
  },
  [OBJECT_FETCH_FULFILLED]: (state, action) => {
    return { ...state, readyState: OBJECT_FETCH_FULFILLED, error: false, object: action.payload.data.data.results[0], status: null, errorObject: null };
  }
};

export default function comics (state = initialState, action) {
  const handler = CRUD_ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
