import { combineReducers } from 'redux';
import comics from './comics';
import creators from './creators';

export default combineReducers({
  comics,
  creators
});
