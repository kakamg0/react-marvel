import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './components/app/Header';
import Comics from './pages/comics/Comics';
import Comic from './pages/comics/Comic';
import Creators from './pages/creators/Creators';
import Creator from './pages/creators/Creator';
import store from './store';

export default function App() {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const theme = React.useMemo(() => createMuiTheme({
    palette: { type: prefersDarkMode ? 'dark' : 'light' },
  }), [ prefersDarkMode ]);

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <CssBaseline/>
        <Router>
          <Header />
          <Switch>
            <Route path="/creators/:id">
              <Creator />
            </Route>
            <Route path="/creators">
              <Creators />
            </Route>
            <Route path="/comics/:id">
              <Comic />
            </Route>
            <Route path="/comics">
              <Comics />
            </Route>
            <Route path="/">
              <Comics />
            </Route>
          </Switch>
        </Router>
      </ThemeProvider>
    </Provider>
  );
}
