import { createStore, compose, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import promise from 'redux-promise-middleware';
import reducers from './reducers';

const middlewares = [ promise, logger ];
const enhancers = [];

if (process.env.NODE_ENV !== 'development') {
  middlewares.pop();
}

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const store = createStore(
  reducers,
  compose(applyMiddleware(...middlewares), ...enhancers)
);

export default store;