import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Box, Typography } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { list, OBJECT_LIST_PENDING, OBJECT_LIST_REJECTED } from '../../actions/comics';
import Loading from '../../components/app/Loading';
import Pagination from '../../components/app/Pagination';
import ComicList from '../../components/comics/ComicList';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  title: {
    margin: theme.spacing(2)
  }
}));

export default function Comics() {
  const classes = useStyles();
  const comics = useSelector(state => state.comics);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(list());
  }, [ dispatch ]);

  const handlePagination = (e, page) => dispatch(list(page));

  if (comics.readyState === OBJECT_LIST_PENDING) {
    return <Loading />;
  }

  if (comics.readyState === OBJECT_LIST_REJECTED) {
    return (
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {comics.errorObject.status}
      </Alert>
    );
  }

  return (
    <div className={classes.root}>
      <Container>
        <Box>
          <Typography className={classes.title} gutterBottom variant="h2" component="h2">
            Comics
          </Typography>
          <ComicList list={comics.list.results} />
          <Pagination
            list={comics.list}
            onChange={handlePagination}
          />
        </Box>
      </Container>
    </div>
  );
}
