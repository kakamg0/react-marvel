import React, { useEffect } from 'react';
import { useParams, Link as RouterLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Box, Typography, Breadcrumbs, Link } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { fetch, OBJECT_FETCH_PENDING, OBJECT_FETCH_REJECTED } from '../../actions/comics';
import Loading from '../../components/app/Loading';
import ComicDetail from '../../components/comics/ComicDetails';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  title: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  breadcrumbs: {
    marginBottom: theme.spacing(2)
  }
}));

export default function Comic() {
  const classes = useStyles();
  const comics = useSelector(state => state.comics);
  const dispatch = useDispatch();
  const { id } = useParams();
  useEffect(() => {
    dispatch(fetch(id));
  }, [ dispatch, id ]);

  if (comics.readyState === OBJECT_FETCH_PENDING) {
    return <Loading />;
  }

  if (comics.readyState === OBJECT_FETCH_REJECTED) {
    return (
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {comics.errorObject.status}
      </Alert>
    );
  }

  return (
    <div className={classes.root}>
      <Container>
        <Box>
          <Typography className={classes.title} gutterBottom variant="h2" component="h2">
            {comics.object.title}
          </Typography>
          <Breadcrumbs className={classes.breadcrumbs} aria-label="breadcrumb">
            <Link component={RouterLink} to="/comics" color="inherit">
              Comics
            </Link>
            <Typography color="textPrimary">Comic</Typography>
          </Breadcrumbs>
          <ComicDetail object={comics.object} />
        </Box>
      </Container>
    </div>
  );
}
