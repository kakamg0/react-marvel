import React, { useEffect } from 'react';
import { useParams, Link as RouterLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Box, Typography, Breadcrumbs, Link } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { fetch, OBJECT_FETCH_PENDING, OBJECT_FETCH_REJECTED } from '../../actions/creators';
import { listByCreator } from '../../actions/comics';
import Loading from '../../components/app/Loading';
import CreatorDetail from '../../components/creators/CreatorDetails';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  title: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  breadcrumbs: {
    marginBottom: theme.spacing(2)
  }
}));

export default function Creator() {
  const classes = useStyles();
  const creators = useSelector(state => state.creators);
  const comics = useSelector(state => state.comics);
  const dispatch = useDispatch();
  const { id } = useParams();
  useEffect(() => {
    dispatch(fetch(id));
    dispatch(listByCreator(id));
  }, [ dispatch, id ]);

  if (creators.readyState === OBJECT_FETCH_PENDING) {
    return <Loading />;
  }

  if (creators.readyState === OBJECT_FETCH_REJECTED) {
    return (
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {creators.errorObject.status}
      </Alert>
    );
  }

  return (
    <div className={classes.root}>
      <Container>
        <Box>
          <Typography className={classes.title} gutterBottom variant="h2" component="h2">
            {creators.object.fullName}
          </Typography>
          <Breadcrumbs className={classes.breadcrumbs} aria-label="breadcrumb">
            <Link component={RouterLink} to="/creators" color="inherit">
              Creators
            </Link>
            <Typography color="textPrimary">Creator</Typography>
          </Breadcrumbs>
          <CreatorDetail object={creators.object} comics={comics.list.results} />
        </Box>
      </Container>
    </div>
  );
}
