## Overview

This repository includes a React application that consumes and displays data from the Marvel API.

## Installation

You have to clone or download this repository to your local filesystem.

Navigate to the root directory and run `npm install`.

## Setup

You have to set your public api key in the REACT_APP_MARVEL_API_KEY environment variable.
You can do this by navigating to the root directory and updating the `.env` file.

To start a local server run `npm start`.

Navigate to [http://localhost:3000](http://localhost:3000) to view it in the browser.
